EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 5B936A0E
P 14800 7900
F 0 "J10" H 14800 8000 50  0000 C CNN
F 1 "Conn_01x01" H 14800 7800 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 14800 7900 50  0001 C CNN
F 3 "" H 14800 7900 50  0001 C CNN
	1    14800 7900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J11
U 1 1 5B9370F8
P 14800 8200
F 0 "J11" H 14800 8300 50  0000 C CNN
F 1 "Conn_01x01" H 14800 8100 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 14800 8200 50  0001 C CNN
F 3 "" H 14800 8200 50  0001 C CNN
	1    14800 8200
	1    0    0    -1  
$EndComp
Text Notes 14550 8500 0    60   ~ 0
screw holes
$Comp
L Connector_Generic:Conn_01x01 J12
U 1 1 5C961332
P 14400 9100
F 0 "J12" H 14400 9200 50  0000 C CNN
F 1 "Conn_01x01" H 14400 9000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 14400 9100 50  0001 C CNN
F 3 "" H 14400 9100 50  0001 C CNN
	1    14400 9100
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J13
U 1 1 5C96182F
P 14700 9100
F 0 "J13" H 14700 9200 50  0000 C CNN
F 1 "Conn_01x01" H 14700 9000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 14700 9100 50  0001 C CNN
F 3 "" H 14700 9100 50  0001 C CNN
	1    14700 9100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C961AC0
P 14400 9350
F 0 "#PWR04" H 14400 9100 50  0001 C CNN
F 1 "GND" H 14400 9200 50  0000 C CNN
F 2 "" H 14400 9350 50  0000 C CNN
F 3 "" H 14400 9350 50  0000 C CNN
	1    14400 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C961BD3
P 14700 9350
F 0 "#PWR05" H 14700 9100 50  0001 C CNN
F 1 "GND" H 14700 9200 50  0000 C CNN
F 2 "" H 14700 9350 50  0000 C CNN
F 3 "" H 14700 9350 50  0000 C CNN
	1    14700 9350
	1    0    0    -1  
$EndComp
NoConn ~ 14600 7900
NoConn ~ 14600 8200
Entry Wire Line
	15200 -700 15300 -600
$Sheet
S 3450 2450 1050 450 
U 5DB5CB48
F0 "Sheet5DB5CB47" 60
F1 "IsolatedSupply.sch" 60
F2 "IsolatedPower12V" I L 3450 2500 60 
F3 "IsolatedPowerGnd" I L 3450 2600 60 
F4 "IsolatedPower5V" O L 3450 2700 60 
$EndSheet
$Sheet
S 9750 3200 1650 2050
U 5DC24124
F0 "Sheet5DC24123" 60
F1 "PowerPath.sch" 60
F2 "DUT+" I R 11400 3350 60 
F3 "CurrentMeas_0.2A" O L 9750 4350 60 
F4 "CurrentMeas_0.02A" O L 9750 4450 60 
F5 "CurrentMeas_2A" O L 9750 4250 60 
F6 "CurrentSet_20A" I L 9750 3300 60 
F7 "CurrentSet_2A" I L 9750 3500 60 
F8 "CurrentSet_0.2A" I L 9750 3700 60 
F9 "CurrentSet_0.02A" I L 9750 3900 60 
F10 "CurrentMeas_20A_1" O L 9750 4550 60 
F11 "CurrentMeas_20A_2" O L 9750 4650 60 
F12 "CurrentMeas_20A_3" O L 9750 4750 60 
F13 "CurrentMeas_20A_4" O L 9750 4850 60 
F14 "~RangeOn_20A" I L 9750 3400 60 
F15 "~RangeOn_0.02A" I L 9750 4000 60 
F16 "~RangeOn_2A" I L 9750 3600 60 
F17 "~RangeOn_0.2A" I L 9750 3800 60 
$EndSheet
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 5DC3123B
P 14800 7600
F 0 "J9" H 14800 7700 50  0000 C CNN
F 1 "Conn_01x01" H 14800 7500 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 14800 7600 50  0001 C CNN
F 3 "" H 14800 7600 50  0001 C CNN
	1    14800 7600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5DC312B1
P 14800 7350
F 0 "J8" H 14800 7450 50  0000 C CNN
F 1 "Conn_01x01" H 14800 7250 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 14800 7350 50  0001 C CNN
F 3 "" H 14800 7350 50  0001 C CNN
	1    14800 7350
	1    0    0    -1  
$EndComp
NoConn ~ 14600 7350
NoConn ~ 14600 7600
$Sheet
S 3450 3250 1450 950 
U 5DC325D7
F0 "Sheet5DC325D6" 60
F1 "IsolatedFanControl.sch" 60
F2 "FanPwm" I R 4900 3450 60 
F3 "FanTacho" O R 4900 3550 60 
F4 "IsolatedPower12V" I L 3450 3350 60 
F5 "IsolatedPowerGnd" I L 3450 4100 60 
F6 "IsolatedPower5V" I L 3450 3450 60 
$EndSheet
$Sheet
S 3450 4400 1450 650 
U 5DC390B9
F0 "Sheet5DC390B8" 60
F1 "IsolatedUSB.sch" 60
F2 "RX" O R 4900 4500 60 
F3 "TX" I R 4900 4600 60 
$EndSheet
$Sheet
S 3450 5300 1200 800 
U 5DC456A4
F0 "Sheet5DC456A3" 60
F1 "IsolatedUart.sch" 60
F2 "TX" I R 4650 5500 60 
F3 "RX" O R 4650 5400 60 
F4 "IsolatedSupply5V" O L 3450 5400 60 
F5 "IsolatedSupplyGnd" O L 3450 5500 60 
F6 "B" O L 3450 5800 60 
F7 "A" O L 3450 5700 60 
F8 "DE~RE" I R 4650 5600 60 
F9 "Trigger" O R 4650 5700 60 
F10 "IsolatedTrigger" I L 3450 5900 60 
$EndSheet
Wire Wire Line
	14700 9300 14700 9350
Wire Wire Line
	14400 9300 14400 9350
Wire Wire Line
	2800 3550 1850 3550
Wire Wire Line
	1750 3650 1950 3650
Wire Wire Line
	1750 3750 2050 3750
Wire Wire Line
	2950 2600 2950 3150
Wire Wire Line
	2950 4100 3450 4100
Wire Wire Line
	2950 2600 3450 2600
Connection ~ 2950 3750
Wire Wire Line
	3200 3450 3200 3650
Wire Wire Line
	3200 3450 3450 3450
Wire Wire Line
	2800 2500 2800 2950
Wire Wire Line
	2800 2500 3450 2500
Wire Wire Line
	3450 3350 2800 3350
Connection ~ 2800 3350
Wire Wire Line
	3200 5400 3450 5400
Connection ~ 3200 3650
Wire Wire Line
	2950 5500 3450 5500
Connection ~ 2950 4100
Wire Wire Line
	2850 5700 3450 5700
Wire Wire Line
	2850 3850 2850 5700
Wire Wire Line
	1750 3850 2150 3850
Wire Wire Line
	3450 5800 2750 5800
Wire Wire Line
	2750 5800 2750 3950
Wire Wire Line
	2750 3950 2250 3950
Wire Wire Line
	2250 4200 2250 3950
Connection ~ 2250 3950
Wire Wire Line
	2150 4200 2150 3850
Connection ~ 2150 3850
Wire Wire Line
	2050 4200 2050 3750
Connection ~ 2050 3750
Wire Wire Line
	1950 4200 1950 3650
Connection ~ 1950 3650
Wire Wire Line
	1850 4200 1850 3550
Connection ~ 1850 3550
$Sheet
S 6700 3000 2300 2850
U 5DC5AE9C
F0 "Sheet5DC5AE9B" 60
F1 "MCU.sch" 60
F2 "FanPwm" O L 6700 3450 60 
F3 "FanTacho" I L 6700 3550 60 
F4 "UartRx" I L 6700 4900 60 
F5 "UsbRx" I L 6700 4500 60 
F6 "UartTx" O L 6700 5000 60 
F7 "UsbTx" O L 6700 4600 60 
F8 "VoltMeas" I R 9000 5100 60 
F9 "DE~RE" O L 6700 5100 60 
F10 "Trigger" I L 6700 5200 60 
F11 "CurrentMeas_0.2A" I R 9000 4350 60 
F12 "CurrentMeas_2A" I R 9000 4250 60 
F13 "CurrentSet_20A" O R 9000 3300 60 
F14 "CurrentSet_2A" O R 9000 3500 60 
F15 "CurrentSet_0.2A" O R 9000 3700 60 
F16 "CurrentSet_0.02A" O R 9000 3900 60 
F17 "CurrentMeas_0.02A" I R 9000 4450 60 
F18 "RangeOn_2A" O R 9000 3600 60 
F19 "RangeOn_0.02A" O R 9000 4000 60 
F20 "RangeOn_20A" O R 9000 3400 60 
F21 "RangeOn_0.2A" O R 9000 3800 60 
F22 "CurrentMeas_20A_4" I R 9000 4850 60 
F23 "CurrentMeas_20A_3" I R 9000 4750 60 
F24 "CurrentMeas_20A_2" I R 9000 4650 60 
F25 "CurrentMeas_20A_1" I R 9000 4550 60 
F26 "SCL" O R 9000 5450 60 
F27 "SDA" B R 9000 5550 60 
$EndSheet
Wire Wire Line
	4900 3550 6700 3550
Wire Wire Line
	6700 3450 4900 3450
Wire Wire Line
	4900 4500 6700 4500
Wire Wire Line
	4900 4600 6700 4600
Wire Wire Line
	4650 5400 5450 5400
Wire Wire Line
	5450 5400 5450 4900
Wire Wire Line
	5450 4900 6700 4900
Wire Wire Line
	6700 5000 5550 5000
Wire Wire Line
	5550 5000 5550 5500
Wire Wire Line
	5550 5500 4650 5500
Wire Wire Line
	9000 3300 9750 3300
Wire Wire Line
	9000 3400 9750 3400
Wire Wire Line
	9750 3500 9000 3500
Wire Wire Line
	9000 3600 9750 3600
Wire Wire Line
	4650 5600 5650 5600
Wire Wire Line
	5650 5600 5650 5100
Wire Wire Line
	5650 5100 6700 5100
Wire Wire Line
	6700 5200 5750 5200
Wire Wire Line
	5750 5200 5750 5700
Wire Wire Line
	5750 5700 4650 5700
Wire Wire Line
	2950 3750 2950 4100
Wire Wire Line
	2800 3350 2800 3550
Wire Wire Line
	3200 3650 3200 5400
Wire Wire Line
	2950 4100 2950 5500
Wire Wire Line
	2250 3950 1750 3950
Wire Wire Line
	2150 3850 2850 3850
Wire Wire Line
	2050 3750 2950 3750
Wire Wire Line
	1950 3650 3200 3650
Wire Wire Line
	1850 3550 1750 3550
$Comp
L Device:R_Small R?
U 1 1 5DF2B73A
P 13150 9350
AR Path="/5DC5AE9C/5DF2B73A" Ref="R?"  Part="1" 
AR Path="/5DF2B73A" Ref="R3"  Part="1" 
F 0 "R3" H 13180 9370 50  0000 L CNN
F 1 "0R" H 13180 9310 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 13150 9350 50  0001 C CNN
F 3 "" H 13150 9350 50  0001 C CNN
	1    13150 9350
	0    -1   1    0   
$EndComp
Wire Wire Line
	12950 9350 13050 9350
$Comp
L power:GND #PWR02
U 1 1 5DF2C99A
P 12950 9350
F 0 "#PWR02" H 12950 9100 50  0001 C CNN
F 1 "GND" H 12950 9200 50  0000 C CNN
F 2 "" H 12950 9350 50  0000 C CNN
F 3 "" H 12950 9350 50  0000 C CNN
	1    12950 9350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR03
U 1 1 5DF2D374
P 13600 9350
F 0 "#PWR03" H 13600 9150 50  0001 C CNN
F 1 "GNDPWR" H 13400 9300 50  0000 C CNN
F 2 "" H 13600 9300 50  0000 C CNN
F 3 "" H 13600 9300 50  0000 C CNN
	1    13600 9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	13250 9350 13600 9350
$Comp
L Graphic:Logo_Open_Hardware_Large #LOGO1
U 1 1 5E478176
P 1850 10250
F 0 "#LOGO1" H 1850 10750 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 1850 9850 50  0001 C CNN
F 2 "" H 1850 10250 50  0001 C CNN
F 3 "~" H 1850 10250 50  0001 C CNN
	1    1850 10250
	1    0    0    -1  
$EndComp
$Sheet
S 3450 1700 1050 400 
U 5E19D482
F0 "NegativeSupply" 60
F1 "NegativeSupply.sch" 60
$EndSheet
$Comp
L Connector:Barrel_Jack_Switch J1
U 1 1 5E1AA827
P 2200 3050
F 0 "J1" H 2257 3367 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 2257 3276 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Wuerth_6941xx301002" H 2250 3010 50  0001 C CNN
F 3 "~" H 2250 3010 50  0001 C CNN
	1    2200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3150 2950 3150
Connection ~ 2950 3150
Wire Wire Line
	2950 3150 2950 3750
Wire Wire Line
	2500 2950 2800 2950
Connection ~ 2800 2950
Wire Wire Line
	2800 2950 2800 3350
$Sheet
S 9750 5600 1650 1500
U 5E25D876
F0 "VoltageSense" 60
F1 "VoltageSense.sch" 60
F2 "EXT-" I R 11400 7050 60 
F3 "EXT+" I R 11400 6500 60 
F4 "voltageMeas" O L 9750 5700 60 
F5 "SCL" I L 9750 5850 60 
F6 "SDA" B L 9750 5950 60 
$EndSheet
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5E312C37
P 13450 6600
AR Path="/5DC24124/5E312C37" Ref="J?"  Part="1" 
AR Path="/5E312C37" Ref="J7"  Part="1" 
F 0 "J7" H 13450 6700 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 13450 6400 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 13450 6600 50  0001 C CNN
F 3 "" H 13450 6600 50  0001 C CNN
	1    13450 6600
	1    0    0    1   
$EndComp
Text Notes 12450 6450 2    60   ~ 0
external voltage sense
$Comp
L Device:D D?
U 1 1 5E312C3E
P 12400 6700
AR Path="/5DC24124/5E312C3E" Ref="D?"  Part="1" 
AR Path="/5E312C3E" Ref="D4"  Part="1" 
F 0 "D4" H 12400 6800 50  0000 C CNN
F 1 "STTH1L06A" H 12400 6600 50  0000 C CNN
F 2 "kicad-footprints-antaveiv:DO-214AC" H 12400 6700 50  0001 C CNN
F 3 "" H 12400 6700 50  0001 C CNN
	1    12400 6700
	0    -1   1    0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5E312C44
P 12100 6700
AR Path="/5DC24124/5E312C44" Ref="D?"  Part="1" 
AR Path="/5E312C44" Ref="D3"  Part="1" 
F 0 "D3" H 12100 6800 50  0000 C CNN
F 1 "CGA0402MLC-24G" H 12100 6600 50  0001 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 12100 6700 50  0001 C CNN
F 3 "" H 12100 6700 50  0001 C CNN
	1    12100 6700
	0    -1   1    0   
$EndComp
Wire Wire Line
	12100 7050 12100 6850
Wire Wire Line
	12400 6850 12400 7050
Connection ~ 12400 7050
Wire Wire Line
	12100 6550 12100 6500
Connection ~ 12100 6500
Wire Wire Line
	12400 6550 12400 6500
Connection ~ 12400 6500
Wire Wire Line
	12400 7050 12100 7050
Wire Wire Line
	12400 6500 12100 6500
Connection ~ 12100 7050
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5E34AC2A
P 13500 3450
AR Path="/5DC24124/5E34AC2A" Ref="J?"  Part="1" 
AR Path="/5E34AC2A" Ref="J2"  Part="1" 
F 0 "J2" H 13500 3550 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 13500 3250 50  0001 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 13500 3450 50  0001 C CNN
F 3 "" H 13500 3450 50  0001 C CNN
	1    13500 3450
	1    0    0    1   
$EndComp
$Comp
L power:GNDPWR #PWR?
U 1 1 5E34AC30
P 12200 3900
AR Path="/5DC24124/5E34AC30" Ref="#PWR?"  Part="1" 
AR Path="/5E34AC30" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 12200 3700 50  0001 C CNN
F 1 "GNDPWR" H 12000 3850 50  0000 C CNN
F 2 "" H 12200 3850 50  0000 C CNN
F 3 "" H 12200 3850 50  0000 C CNN
	1    12200 3900
	-1   0    0    -1  
$EndComp
Text Notes 13450 3200 2    60   ~ 0
power connector
Text Label 11800 3350 0    60   ~ 0
Load
$Comp
L Device:D D?
U 1 1 5E34AC38
P 12500 3600
AR Path="/5DC24124/5E34AC38" Ref="D?"  Part="1" 
AR Path="/5E34AC38" Ref="D2"  Part="1" 
F 0 "D2" H 12500 3700 50  0000 C CNN
F 1 "STTH1L06A" H 12500 3500 50  0000 C CNN
F 2 "kicad-footprints-antaveiv:DO-214AC" H 12500 3600 50  0001 C CNN
F 3 "" H 12500 3600 50  0001 C CNN
	1    12500 3600
	0    -1   1    0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 5E34AC3E
P 12200 3600
AR Path="/5DC24124/5E34AC3E" Ref="D?"  Part="1" 
AR Path="/5E34AC3E" Ref="D1"  Part="1" 
F 0 "D1" H 12200 3700 50  0000 C CNN
F 1 "CGA0402MLC-24G" H 12200 3500 50  0001 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 12200 3600 50  0001 C CNN
F 3 "" H 12200 3600 50  0001 C CNN
	1    12200 3600
	0    -1   1    0   
$EndComp
Wire Wire Line
	13300 3450 13300 3850
Wire Wire Line
	13300 3350 13150 3350
Wire Wire Line
	12500 3450 12500 3350
Connection ~ 12500 3350
Wire Wire Line
	12500 3750 12500 3850
Wire Wire Line
	13300 3850 13000 3850
Wire Wire Line
	12200 3450 12200 3350
Connection ~ 12200 3350
Wire Wire Line
	12200 3850 12200 3750
Connection ~ 12500 3850
Wire Wire Line
	12500 3350 12200 3350
Wire Wire Line
	12500 3850 12200 3850
Wire Wire Line
	9400 5700 9750 5700
Wire Wire Line
	12400 6500 12550 6500
Wire Wire Line
	13250 6600 13250 7050
Wire Wire Line
	12400 7050 12550 7050
$Comp
L Device:R_Small R?
U 1 1 5E274FA7
P 12650 7050
AR Path="/5DC24124/5E274FA7" Ref="R?"  Part="1" 
AR Path="/5E274FA7" Ref="R2"  Part="1" 
F 0 "R2" H 12680 7070 50  0000 L CNN
F 1 "10K" H 12680 7010 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 12650 7050 50  0001 C CNN
F 3 "" H 12650 7050 50  0000 C CNN
	1    12650 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12750 7050 13000 7050
$Comp
L Device:R_Small R?
U 1 1 5E27B25D
P 12650 6500
AR Path="/5DC24124/5E27B25D" Ref="R?"  Part="1" 
AR Path="/5E27B25D" Ref="R1"  Part="1" 
F 0 "R1" H 12680 6520 50  0000 L CNN
F 1 "10K" H 12680 6460 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 12650 6500 50  0001 C CNN
F 3 "" H 12650 6500 50  0000 C CNN
	1    12650 6500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	12750 6500 13150 6500
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5E281098
P 13350 4650
F 0 "J5" H 13430 4642 50  0000 L CNN
F 1 "Conn_01x02" H 13430 4551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 13350 4650 50  0001 C CNN
F 3 "~" H 13350 4650 50  0001 C CNN
	1    13350 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5E281386
P 12800 4750
F 0 "J6" H 12718 4425 50  0000 C CNN
F 1 "Conn_01x02" H 12718 4516 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 12800 4750 50  0001 C CNN
F 3 "~" H 12800 4750 50  0001 C CNN
	1    12800 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	13150 3350 13150 4650
Connection ~ 13150 3350
Wire Wire Line
	13150 3350 12500 3350
Wire Wire Line
	13150 4750 13150 6500
Connection ~ 13150 6500
Wire Wire Line
	13150 6500 13250 6500
Wire Wire Line
	13000 4650 13000 3850
Connection ~ 13000 3850
Wire Wire Line
	13000 3850 12500 3850
Wire Wire Line
	13000 4750 13000 7050
Connection ~ 13000 7050
Wire Wire Line
	13000 7050 13250 7050
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5E29B743
P 1550 3850
F 0 "J3" H 1468 3325 50  0000 C CNN
F 1 "Conn_01x06" H 1468 3416 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1550 3850 50  0001 C CNN
F 3 "~" H 1550 3850 50  0001 C CNN
	1    1550 3850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 5E29BFC6
P 2150 4400
F 0 "J4" V 2022 4680 50  0000 L CNN
F 1 "Conn_01x06" V 2113 4680 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 2150 4400 50  0001 C CNN
F 3 "~" H 2150 4400 50  0001 C CNN
	1    2150 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 4050 2350 4050
Wire Wire Line
	2350 4050 2350 4200
Wire Wire Line
	2350 4050 2650 4050
Wire Wire Line
	2650 4050 2650 5900
Wire Wire Line
	2650 5900 3450 5900
Connection ~ 2350 4050
$Comp
L Connector_Generic:Conn_01x01 J25
U 1 1 5E4FAB7E
P 14100 9100
F 0 "J25" H 14100 9200 50  0000 C CNN
F 1 "Conn_01x01" H 14100 9000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 14100 9100 50  0001 C CNN
F 3 "" H 14100 9100 50  0001 C CNN
	1    14100 9100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5E4FAB84
P 14100 9350
F 0 "#PWR0124" H 14100 9100 50  0001 C CNN
F 1 "GND" H 14100 9200 50  0000 C CNN
F 2 "" H 14100 9350 50  0000 C CNN
F 3 "" H 14100 9350 50  0000 C CNN
	1    14100 9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	14100 9300 14100 9350
Wire Wire Line
	3200 3450 3200 2700
Wire Wire Line
	3200 2700 3450 2700
Connection ~ 3200 3450
$Comp
L Connector_Generic:Conn_01x01 J26
U 1 1 5F142712
P 13850 9100
F 0 "J26" H 13850 9200 50  0000 C CNN
F 1 "Conn_01x01" H 13850 9000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 13850 9100 50  0001 C CNN
F 3 "" H 13850 9100 50  0001 C CNN
	1    13850 9100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 5F142988
P 13850 9350
F 0 "#PWR0147" H 13850 9100 50  0001 C CNN
F 1 "GND" H 13850 9200 50  0000 C CNN
F 2 "" H 13850 9350 50  0000 C CNN
F 3 "" H 13850 9350 50  0000 C CNN
	1    13850 9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	13850 9300 13850 9350
Wire Wire Line
	9000 3700 9750 3700
Wire Wire Line
	9750 3800 9000 3800
NoConn ~ 2500 3050
Wire Wire Line
	9000 4250 9750 4250
Wire Wire Line
	9000 4350 9750 4350
Wire Wire Line
	9000 4450 9750 4450
Wire Wire Line
	9750 3900 9000 3900
Wire Wire Line
	9000 4000 9750 4000
Wire Wire Line
	9000 4550 9750 4550
Wire Wire Line
	9750 4650 9000 4650
Wire Wire Line
	9000 4750 9750 4750
Wire Wire Line
	9000 4850 9750 4850
Wire Wire Line
	9000 5100 9400 5100
Wire Wire Line
	9400 5100 9400 5700
$Comp
L Connector_Generic:Conn_01x01 J16
U 1 1 60BCB442
P 14800 7100
F 0 "J16" H 14800 7200 50  0000 C CNN
F 1 "Conn_01x01" H 14800 7000 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 14800 7100 50  0001 C CNN
F 3 "" H 14800 7100 50  0001 C CNN
	1    14800 7100
	1    0    0    -1  
$EndComp
NoConn ~ 14600 7100
Wire Wire Line
	11400 6500 12100 6500
Wire Wire Line
	11400 7050 12100 7050
Wire Wire Line
	11400 3350 12200 3350
Wire Wire Line
	12200 3850 12200 3900
Connection ~ 12200 3850
Wire Wire Line
	9000 5450 9350 5450
Wire Wire Line
	9350 5450 9350 5850
Wire Wire Line
	9350 5850 9750 5850
Wire Wire Line
	9750 5950 9300 5950
Wire Wire Line
	9300 5950 9300 5550
Wire Wire Line
	9300 5550 9000 5550
$EndSCHEMATC
