EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8400 4650 2    60   ~ 0
12V 4-pin PC fan connector
$Comp
L Device:C_Small C30
U 1 1 5DC328B6
P 7550 4150
F 0 "C30" H 7560 4220 50  0000 L CNN
F 1 "1uF" H 7560 4070 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7550 4150 50  0001 C CNN
F 3 "" H 7550 4150 50  0001 C CNN
	1    7550 4150
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J17
U 1 1 5DC328BA
P 7950 4000
F 0 "J17" H 7950 4200 50  0000 C CNN
F 1 "Conn_01x04" H 7950 3700 50  0000 C CNN
F 2 "Connector:FanPinHeader_1x04_P2.54mm_Vertical" H 7950 4000 50  0001 C CNN
F 3 "" H 7950 4000 50  0001 C CNN
	1    7950 4000
	1    0    0    1   
$EndComp
Wire Wire Line
	7500 4000 7550 4000
Text HLabel 3300 3450 0    60   Input ~ 0
FanPwm
$Comp
L Device:R_Small R46
U 1 1 5DC328BC
P 5700 3300
F 0 "R46" H 5730 3320 50  0000 L CNN
F 1 "10K_NM" H 5730 3260 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 3300 50  0001 C CNN
F 3 "" H 5700 3300 50  0001 C CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3900 7750 3900
$Comp
L Device:R_Small R47
U 1 1 5DC328BE
P 6750 3550
F 0 "R47" H 6780 3570 50  0000 L CNN
F 1 "10K_NM" H 6780 3510 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6750 3550 50  0001 C CNN
F 3 "" H 6750 3550 50  0001 C CNN
	1    6750 3550
	0    -1   1    0   
$EndComp
Wire Wire Line
	6950 3550 6850 3550
Wire Wire Line
	4850 3550 5700 3550
Wire Wire Line
	5700 3400 5700 3550
Text HLabel 3300 3550 0    60   Output ~ 0
FanTacho
Connection ~ 5700 3550
Text HLabel 7500 4000 0    60   Input ~ 0
IsolatedPower12V
Text HLabel 5000 4450 0    60   Input ~ 0
IsolatedPowerGnd
$Comp
L Isolator:ISO7320C U9
U 1 1 5DC3399D
P 4450 3350
F 0 "U9" H 4450 3775 50  0000 C CNN
F 1 "SI8621AB-B-IS" H 4450 3700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4450 3000 50  0001 C CIN
F 3 "" H 4450 3350 50  0001 C CNN
	1    4450 3350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V0 #PWR050
U 1 1 5DC3399E
P 4000 2650
F 0 "#PWR050" H 4000 2500 50  0001 C CNN
F 1 "+3V0" H 4000 2790 50  0000 C CNN
F 2 "" H 4000 2650 50  0000 C CNN
F 3 "" H 4000 2650 50  0000 C CNN
	1    4000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3150 4000 3150
Wire Wire Line
	4000 3150 4000 2700
Wire Wire Line
	5000 4450 5250 4450
Text HLabel 7250 2950 2    60   Input ~ 0
IsolatedPower5V
Wire Wire Line
	4850 2950 5250 2950
Wire Wire Line
	7550 4000 7550 4050
Connection ~ 7550 4000
Wire Wire Line
	7750 4450 7750 4100
Wire Wire Line
	7550 4250 7550 4450
Connection ~ 7550 4450
Text Notes 4450 2050 0    60   ~ 0
Fan consumes high current, high power isolated \nDC/DC converter would be needed to not isolate here.\nIt's cheaper to isolate the control signals
Wire Wire Line
	3300 3450 3350 3450
Wire Wire Line
	3300 3550 3350 3550
Connection ~ 5700 2950
Wire Wire Line
	6950 3900 6950 3550
Wire Wire Line
	7450 3450 7450 3800
Wire Wire Line
	7450 3800 7750 3800
Wire Wire Line
	4850 3250 5250 3250
Wire Wire Line
	5250 3250 5250 4450
Connection ~ 5250 4450
Wire Wire Line
	5700 3550 6150 3550
Wire Wire Line
	7550 4000 7750 4000
Wire Wire Line
	7550 4450 7750 4450
Wire Wire Line
	5700 2950 7250 2950
$Comp
L Connector:TestPoint_Alt TP?
U 1 1 5E460267
P 3350 3400
AR Path="/5DB5CB48/5E460267" Ref="TP?"  Part="1" 
AR Path="/5DC325D7/5E460267" Ref="TP16"  Part="1" 
F 0 "TP16" H 3350 3700 50  0000 C BNN
F 1 "TEST" H 3350 3650 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3350 3400 50  0001 C CNN
F 3 "" H 3350 3400 50  0001 C CNN
	1    3350 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3400 3350 3450
Connection ~ 3350 3450
Wire Wire Line
	3350 3450 4050 3450
$Comp
L Connector:TestPoint_Alt TP?
U 1 1 5E462C74
P 3350 3600
AR Path="/5DB5CB48/5E462C74" Ref="TP?"  Part="1" 
AR Path="/5DC325D7/5E462C74" Ref="TP17"  Part="1" 
F 0 "TP17" H 3350 3900 50  0000 C BNN
F 1 "TEST" H 3350 3850 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3350 3600 50  0001 C CNN
F 3 "" H 3350 3600 50  0001 C CNN
	1    3350 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 3550 3350 3600
Connection ~ 3350 3550
Wire Wire Line
	3350 3550 4050 3550
$Comp
L Device:C_Small C?
U 1 1 5E5A39D4
P 3750 2850
AR Path="/5E56828B/5E5A39D4" Ref="C?"  Part="1" 
AR Path="/5E568147/5E5A39D4" Ref="C?"  Part="1" 
AR Path="/5DC456A4/5E5A39D4" Ref="C?"  Part="1" 
AR Path="/5DC325D7/5E5A39D4" Ref="C69"  Part="1" 
F 0 "C69" H 3760 2920 50  0000 L CNN
F 1 "0.1uF" H 3760 2770 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3750 2850 50  0001 C CNN
F 3 "" H 3750 2850 50  0001 C CNN
	1    3750 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5E5A56F8
P 3750 3250
F 0 "#PWR0127" H 3750 3000 50  0001 C CNN
F 1 "GND" H 3750 3100 50  0000 C CNN
F 2 "" H 3750 3250 50  0001 C CNN
F 3 "" H 3750 3250 50  0001 C CNN
	1    3750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3250 3750 2950
Wire Wire Line
	3750 2750 3750 2700
Wire Wire Line
	3750 2700 4000 2700
Connection ~ 4000 2700
Wire Wire Line
	4000 2700 4000 2650
Wire Wire Line
	4050 3250 3750 3250
Connection ~ 3750 3250
Wire Wire Line
	5250 4450 5700 4450
$Comp
L Device:R_Small R72
U 1 1 602D1ED1
P 5600 3850
F 0 "R72" H 5630 3870 50  0000 L CNN
F 1 "10K_NM" H 5630 3810 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5600 3850 50  0001 C CNN
F 3 "" H 5600 3850 50  0001 C CNN
	1    5600 3850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R78
U 1 1 602D220D
P 5700 3850
F 0 "R78" H 5730 3870 50  0000 L CNN
F 1 "10K_NM" H 5730 3810 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5700 3850 50  0001 C CNN
F 3 "" H 5700 3850 50  0001 C CNN
	1    5700 3850
	1    0    0    1   
$EndComp
Wire Wire Line
	5700 3550 5700 3700
Wire Wire Line
	5600 3750 5600 3700
Wire Wire Line
	5600 3700 5700 3700
Connection ~ 5700 3700
Wire Wire Line
	5700 3700 5700 3750
Wire Wire Line
	5700 3950 5700 4000
Connection ~ 5700 4450
Wire Wire Line
	5700 4450 6150 4450
Wire Wire Line
	5600 3950 5600 4000
Wire Wire Line
	5600 4000 5700 4000
Connection ~ 5700 4000
Wire Wire Line
	5700 4000 5700 4450
$Comp
L Device:R_Small R102
U 1 1 609F584D
P 7150 3450
F 0 "R102" H 7180 3470 50  0000 L CNN
F 1 "100R" H 7180 3410 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7150 3450 50  0001 C CNN
F 3 "" H 7150 3450 50  0001 C CNN
	1    7150 3450
	0    -1   1    0   
$EndComp
Wire Wire Line
	7250 3450 7450 3450
Text Notes 9950 3950 2    60   ~ 0
Not connected until implemented in FW. 
$Comp
L Device:C_Small C?
U 1 1 60A09706
P 5250 3100
AR Path="/5E56828B/60A09706" Ref="C?"  Part="1" 
AR Path="/5E568147/60A09706" Ref="C?"  Part="1" 
AR Path="/5DC456A4/60A09706" Ref="C?"  Part="1" 
AR Path="/5DC325D7/60A09706" Ref="C75"  Part="1" 
F 0 "C75" H 5260 3170 50  0000 L CNN
F 1 "0.1uF" H 5260 3020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5250 3100 50  0001 C CNN
F 3 "" H 5250 3100 50  0001 C CNN
	1    5250 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3200 5250 3250
Connection ~ 5250 3250
Wire Wire Line
	5250 3000 5250 2950
Connection ~ 5250 2950
Wire Wire Line
	5250 2950 5700 2950
Wire Wire Line
	4850 2950 4850 3150
Wire Wire Line
	5700 2950 5700 3200
$Comp
L Device:D_TVS D?
U 1 1 60BBE64E
P 6150 3850
AR Path="/5DC5AE9C/60BBE64E" Ref="D?"  Part="1" 
AR Path="/5DC390B9/60BBE64E" Ref="D?"  Part="1" 
AR Path="/5DC325D7/60BBE64E" Ref="D21"  Part="1" 
F 0 "D21" H 6150 3950 50  0000 C CNN
F 1 "CGA0402MLC-05G" H 6150 3750 50  0001 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6150 3850 50  0001 C CNN
F 3 "" H 6150 3850 50  0001 C CNN
	1    6150 3850
	0    1    -1   0   
$EndComp
$Comp
L Device:D_TVS D?
U 1 1 60BBF2A7
P 6350 3850
AR Path="/5DC5AE9C/60BBF2A7" Ref="D?"  Part="1" 
AR Path="/5DC390B9/60BBF2A7" Ref="D?"  Part="1" 
AR Path="/5DC325D7/60BBF2A7" Ref="D22"  Part="1" 
F 0 "D22" H 6350 3950 50  0000 C CNN
F 1 "CGA0402MLC-05G" H 6350 3750 50  0001 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6350 3850 50  0001 C CNN
F 3 "" H 6350 3850 50  0001 C CNN
	1    6350 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 4000 6150 4450
Connection ~ 6150 4450
Wire Wire Line
	6150 4450 6350 4450
Wire Wire Line
	6350 4000 6350 4450
Connection ~ 6350 4450
Wire Wire Line
	6350 4450 7550 4450
Wire Wire Line
	6350 3450 6350 3700
Connection ~ 6350 3450
Wire Wire Line
	6350 3450 7050 3450
Wire Wire Line
	4850 3450 6350 3450
Wire Wire Line
	6150 3550 6150 3700
Connection ~ 6150 3550
Wire Wire Line
	6150 3550 6650 3550
$EndSCHEMATC
